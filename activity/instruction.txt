Instructions:

	1. In the S09 folder, create an activity folder.
	2. Clone/copy the template for the index.html file.
	3. Apply Bootstrap to the project by adding all the dependencies.
	4. Apply Bootstrap classes to the outer div tag to create a fluid container, center the text elements and add a padding.
	5. Apply Bootstrap classes to the Web API Ecommerce and React Frontend Section with the following layout:
		- When in a large screen, the contents of the columns at each row should be alternating.
		- At first row, the left side contains the text and the right side contains the image.
		- At second row, the left side contains the image and the right side contains the text.
		- When in a small screen, the image should be shown first before the text.
	6. Apply Bootstrap classes to the Other Projects Section with the following layout:
		- The images and their corresponding headers and texts should all appear in a column.
		- Each set of images and texts should be equal in width and should appear beside each other.
	7. Create a git repository named S09.
	8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	9. Add the link in Boodle.
